FROM openjdk:8u121-jdk
VOLUME /tmp
ADD /target/Infosys-DevOps-ANZ-0.0.1-SNAPSHOT.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]